package com.app.renthouses.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.renthouses.models.entity.Users;
import com.app.renthouses.models.service.IUsersService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })
@RestController
@RequestMapping("/api/user")
public class UserController {

	@Autowired
	private IUsersService iUsersService;
	
	@Autowired 
	private BCryptPasswordEncoder passwordEncoder;

	@GetMapping(value = "/list")
	@Secured({ "ROLE_ADMIN", "ROLE_USER" })
	public List<Users> list() {
		return iUsersService.usersActive();
	}

	@RequestMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
	@Secured({ "ROLE_ADMIN", "ROLE_USER" })
	public Map<String, String> save(@RequestBody Users user, HttpServletRequest request,
			HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException {

		/*{
        "id": 1,
        "name": "Luisa",
        "lastName": "Leins",
        "identification": "17987456",
        "username": "user@rh.com",
        "cellphone": "+584248521463",
        "phone": "+587412563",
        "estate": {
            "id": 1,
            "name": "Caracas",
            "status": true,
            "location": [
                {
                    "id": 1,
                    "name": "Plaza Vzla",
                    "status": true
                },
                {
                    "id": 2,
                    "name": "Santa Monica",
                    "status": true
                }
            ]
        },
        "gender": {
            "id": 2,
            "name": "Female",
            "status": true
        },
        "role": {
            "id": 1,
            "role": "ROLE_USER",
            "status": true
        },
        "status": true
    }*/

		HashMap<String, String> map = new HashMap<>();
		//GenerateCodeQR codeQR = new GenerateCodeQR();

		//try {
			
			/*Integer randomNum = ThreadLocalRandom.current().nextInt(100000, 900000);
			//System.out.println("randomNum "+randomNum);
			String token = passwordEncoder.encode(randomNum.toString());
			//System.out.println("bcryptCode "+token);
			user.setToken(token);*/
			user.setPassword(passwordEncoder.encode(user.getPassword()));
			user.setStatus(true);
			iUsersService.save(user);
			map.put("mensaje", "Guardado con Éxito");
			
			/*Email sendEmail = new Email();
			sendEmail.welcomeMail(users.getUsername(), users.getName()+" "+users.getLastName(), "", "");*/

			return map;

		/*} catch (Exception e) {
			// TODO: handle exception
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Error guardando el registro, revise los datos ");
			map.put("error", "Error guardando el registro, revise los datos");
			return map;
		}*/

	}
}
