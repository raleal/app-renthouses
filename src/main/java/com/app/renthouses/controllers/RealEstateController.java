package com.app.renthouses.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.renthouses.models.entity.RealEstate;
import com.app.renthouses.models.service.IRealEstateService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })
@RestController
@RequestMapping("/api/realEstate")
public class RealEstateController {

	@Autowired
	private IRealEstateService iRealEstateService;

	@GetMapping(value = "/list")
	@Secured({ "ROLE_ADMIN", "ROLE_USER" })
	public List<RealEstate> listar() {
		return iRealEstateService.realEstateActive();
	}

	@RequestMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
	@Secured({ "ROLE_ADMIN" })
	public Map<String, String> save(@RequestBody RealEstate realEstate, HttpServletRequest request,
			HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException {

		HashMap<String, String> map = new HashMap<>();

		try {
			
			/*{
        "id": 1,
        "name": "Garden",
        "address": "St one",
        "mts2": 100.0,
        "latitude": "110.36.25.14",
        "longitude": "230.96.85.74",
        "description": "New",
        "amount": 5000.0,
        "parking": 2,
        "status": {
            "id": 1,
            "name": "Active",
            "status": true
        },
        "type": {
            "id": 1,
            "name": "House",
            "status": true
        },
        "buySell": {
            "id": 3,
            "name": "Rent",
            "status": true
        },
        "usersPlan": {
            "id": 1,
            "users": {
                "id": 1,
                "name": "Luisa",
                "lastName": "Leins",
                "username": "user@rh.com",
                "status": true,
                "role": {
                    "id": 1,
                    "role": "ROLE_USER",
                    "status": true
                }
            },
            "plan": {
                "id": 1,
                "name": "Gold",
                "amount": 10.0,
                "km": 5.0,
                "status": true
            }
        },
        "estate": {
            "id": 1,
            "name": "Caracas",
            "status": true,
            "location": [
                {
                    "id": 1,
                    "name": "Plaza Vzla",
                    "status": true
                },
                {
                    "id": 2,
                    "name": "Santa Monica",
                    "status": true
                }
            ]
        },
        "areaRealEstate": [
            {
                "id": 1,
                "area": {
                    "id": 1,
                    "name": "Bed Room",
                    "status": true
                },
                "count": 2
            },
            {
                "id": 2,
                "area": {
                    "id": 2,
                    "name": "Bath Room",
                    "status": true
                },
                "count": 2
            }
        ],
        "placeRealEstate": [
            {
                "id": 1,
                "place": {
                    "id": 1,
                    "name": "Panaderia",
                    "status": true
                }
            },
            {
                "id": 2,
                "place": {
                    "id": 2,
                    "name": "Super Mercado",
                    "status": true
                }
            }
        ]
    }*/

			iRealEstateService.save(realEstate);
			map.put("mensaje", "Guardado con Éxito");

			/*Email sendEmail = new Email();
			sendEmail.welcomeMail(commerce.getEmail(), commerce.getFullName(), commerce.getNameCommerce(), commerce.getIdentification());*/

			return map;

		} catch (Exception e) {
			// TODO: handle exception
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Error guardando el registro, revise los datos ");
			map.put("error", "Error guardando el registro, revise los datos");
			return map;
		}

	}
}
