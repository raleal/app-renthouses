package com.app.renthouses.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.app.renthouses.models.entity.RealEstate;

public interface IRealEstateDao extends CrudRepository<RealEstate, Long>{
	
	@Query(value = "SELECT * FROM real_estate re WHERE re.status_id = 1", nativeQuery = true)
	public List<RealEstate> realEstateActive();

}
