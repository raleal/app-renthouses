package com.app.renthouses.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.renthouses.models.dao.IRealEstateDao;
import com.app.renthouses.models.entity.RealEstate;
import com.app.renthouses.models.service.IRealEstateService;

@Service
public class IRealEstateImpl implements IRealEstateService{
	
	@Autowired
	private IRealEstateDao iRealEstateDao;

	@Override
	public List<RealEstate> findAll() {
		// TODO Auto-generated method stub
		return (List<RealEstate>) iRealEstateDao.findAll();
	}

	@Override
	public List<RealEstate> realEstateActive() {
		// TODO Auto-generated method stub
		return (List<RealEstate>) iRealEstateDao.realEstateActive();
	}

	@Override
	public void save(RealEstate realEstate) {
		// TODO Auto-generated method stub
		iRealEstateDao.save(realEstate);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}
	

}
