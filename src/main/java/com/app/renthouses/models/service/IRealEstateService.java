package com.app.renthouses.models.service;

import java.util.List;

import com.app.renthouses.models.entity.RealEstate;

public interface IRealEstateService {
	
	public List<RealEstate> findAll();
	
	public List<RealEstate> realEstateActive();
	
	public void save(RealEstate realEstate);
	
	public void delete(Long id);

}
