package com.app.renthouses.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.renthouses.models.dao.IUsersDao;
import com.app.renthouses.models.entity.Users;
import com.app.renthouses.models.service.IUsersService;

@Service
public class IUsersImpl implements IUsersService{
	
	@Autowired
	private IUsersDao iUsersDao;
	
	@Override
	public Users findByUsername(String username) {
		// TODO Auto-generated method stub
		return iUsersDao.findByUsername(username);
	}

	@Override
	public List<Users> findAll() {
		// TODO Auto-generated method stub
		return (List<Users>) iUsersDao.findAll();
	}

	@Override
	public void save(Users user) {
		// TODO Auto-generated method stub
		iUsersDao.save(user);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Users> usersActive() {
		// TODO Auto-generated method stub
		List<Users> users = iUsersDao.usersActive();
		for(int i=0;i<users.size();i++) {
			users.get(i).setPassword(null);
		}
		return users; 
	}

	@Override
	public void updateToken(String token, Long id) {
		// TODO Auto-generated method stub
		iUsersDao.updateToken(token, id);
	}

}
