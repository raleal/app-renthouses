package com.app.renthouses.models.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "place_real_estate")
public class PlaceRealEstate implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="place_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Place place;
	
	/*@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="real_estate_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private RealEstate realEstate;*/
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	/*public RealEstate getRealEstate() {
		return realEstate;
	}*/

	
	/*public void setRealEstate(RealEstate realEstate) {
		this.realEstate = realEstate;
	}*/

	public Place getPlace() {
		return place;
	}

	public void setPlace(Place place) {
		this.place = place;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
