package com.app.renthouses.models.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "users_plan")
public class UsersPlan implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="users_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Users users;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="plan_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Plan plan;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Users getUsers() {
		return users;
	}

	public Plan getPlan() {
		return plan;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
