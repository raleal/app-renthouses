package com.app.renthouses.models.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "real_estate")
public class RealEstate implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String name;
	
	private String address;

	private Double mts2;

	private String latitude;
	
	private String longitude;
	
	private String description;
	
	private Double amount;
	
	private Integer parking;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="status_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Status status;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="type_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Type type;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="buy_sell_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private BuySell buySell;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="users_plan_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private UsersPlan usersPlan;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="estate_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Estate estate;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "real_estate_id")
	private List<AreaRealEstate> areaRealEstate;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "real_estate_id")
	private List<PlaceRealEstate> placeRealEstate;

	public RealEstate() {
		this.areaRealEstate = new ArrayList<AreaRealEstate>();
		this.placeRealEstate = new ArrayList<PlaceRealEstate>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public Double getMts2() {
		return mts2;
	}

	public String getLatitude() {
		return latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public String getDescription() {
		return description;
	}

	public Double getAmount() {
		return amount;
	}

	public Status getStatus() {
		return status;
	}

	public Type getType() {
		return type;
	}

	public BuySell getBuySell() {
		return buySell;
	}

	public UsersPlan getUsersPlan() {
		return usersPlan;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setMts2(Double mts2) {
		this.mts2 = mts2;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Integer getParking() {
		return parking;
	}

	public void setParking(Integer parking) {
		this.parking = parking;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public void setBuySell(BuySell buySell) {
		this.buySell = buySell;
	}

	public void setUsersPlan(UsersPlan usersPlan) {
		this.usersPlan = usersPlan;
	}


	public List<AreaRealEstate> getAreaRealEstate() {
		return areaRealEstate;
	}

	public void setAreaRealEstate(List<AreaRealEstate> areaRealEstate) {
		this.areaRealEstate = areaRealEstate;
	}


	public Estate getEstate() {
		return estate;
	}

	public List<PlaceRealEstate> getPlaceRealEstate() {
		return placeRealEstate;
	}

	public void setEstate(Estate estate) {
		this.estate = estate;
	}

	public void setPlaceRealEstate(List<PlaceRealEstate> placeRealEstate) {
		this.placeRealEstate = placeRealEstate;
	}


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
