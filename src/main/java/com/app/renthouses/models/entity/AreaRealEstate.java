package com.app.renthouses.models.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "area_real_estate")
public class AreaRealEstate implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="area_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Area area;
	
	/*@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="real_estate_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private RealEstate realEstate;*/
	
	private Integer count;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Area getArea() {
		return area;
	}

	/*public RealEstate getRealEstate() {
		return realEstate;
	}*/

	public Integer getCount() {
		return count;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	/*public void setRealEstate(RealEstate realEstate) {
		this.realEstate = realEstate;
	}*/

	public void setCount(Integer count) {
		this.count = count;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
