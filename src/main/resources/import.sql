/* Creamos algunos usuarios con sus roles */
INSERT INTO `role` (role, status) VALUES ('ROLE_USER', 1);
INSERT INTO `role` (role, status) VALUES ('ROLE_ADMIN', 1);

INSERT INTO `estate` (name, status) VALUES ('Caracas', 1);
INSERT INTO `estate` (name, status) VALUES ('Miranda', 1);

INSERT INTO `location` (name, status, estate_id) VALUES ('Plaza Vzla', 1, 1);
INSERT INTO `location` (name, status, estate_id) VALUES ('Santa Monica', 1, 1);
INSERT INTO `location` (name, status, estate_id) VALUES ('Macaracuay', 1, 2);
INSERT INTO `location` (name, status, estate_id) VALUES ('Guatire', 1, 2);

INSERT INTO `gender` (name, status) VALUES ('Male', 1);
INSERT INTO `gender` (name, status) VALUES ('Female', 1);

INSERT INTO `users` (name, last_name, username, password, identification, cellphone, phone, status, estate_id, gender_id, role_id) VALUES ('Luisa','Leins','user@rh.com','$2a$10$O9wxmH/AeyZZzIS09Wp8YOEMvFnbRVJ8B4dmAMVSGloR62lj.yqXG',17987456,'+584248521463','+587412563',1,1,2,1);
INSERT INTO `users` (name, last_name, username, password, identification, cellphone, phone, status, estate_id, gender_id, role_id) VALUES ('Mery','Jane','admin@rh.com','$2a$10$DOMDxjYyfZ/e7RcBfUpzqeaCs8pLgcizuiQWXPkU35nOhZlFcE9MS',19526478,'+584124587896','+586725874',1,1,2,2);

INSERT INTO `area` (name, status) VALUES ('Bed Room', 1);
INSERT INTO `area` (name, status) VALUES ('Bath Room', 1);

INSERT INTO `plan` (name, status, amount, km) VALUES ('Gold', 1, 10, 5);
INSERT INTO `plan` (name, status, amount, km) VALUES ('Silver', 1, 5, 2);

INSERT INTO `status` (name, status) VALUES ('Active', 1);
INSERT INTO `status` (name, status) VALUES ('Inactive', 1);

INSERT INTO `type` (name, status) VALUES ('House', 1);
INSERT INTO `type` (name, status) VALUES ('Office', 1);

INSERT INTO `buy_sell` (name, status) VALUES ('Buy', 1);
INSERT INTO `buy_sell` (name, status) VALUES ('Sell', 1);
INSERT INTO `buy_sell` (name, status) VALUES ('Rent', 1);

INSERT INTO `place` (name, status) VALUES ('Panaderia', 1);
INSERT INTO `place` (name, status) VALUES ('Super Mercado', 1);



